FROM python:3.6

ADD pastr /opt/pastr
COPY requirements.txt /opt/
RUN pip install -r /opt/requirements.txt

CMD DB_SERVER=$DB_SERVER python /opt/pastr/__init__.py
