from flask import Flask, request, render_template, Blueprint
from flask_restful import Resource, Api
import redis
import uuid
import os
import json


web_blueprint = Blueprint('web', __name__, template_folder='web')

def create_app():
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(PasteList, '/api/')
    api.add_resource(Paste, '/api/<paste_id>')

    app.config["DB_SERVER"] = os.environ.get("DB_SERVER", default="localhost")

    app.db = redis.StrictRedis(host=app.config["DB_SERVER"], port=6379, db=0)

    app.register_blueprint(web_blueprint)

    return app,api

class Paste(Resource):
    def get(self, paste_id):
        return app.db.get(paste_id).decode("utf-8", "replace")

    def put(self, paste_id):
        app.db.set(paste_id, request.data)
        return str(app.db.get(paste_id))

    def delete(self, paste_id):
        old_content = app.db.get(paste_id)
        app.db.delete(paste_id)
        return old_content.decode("utf-8", "replace")

class PasteList(Resource):

    def get(self):
        app.logger.debug("GET PasteList")
        return [k.decode('utf-8', "replace") for k in app.db.keys()]

    def post(self):
        id = str(uuid.uuid1())
        app.db.set(id, request.data)
        return id

@web_blueprint.route("/")
def main_view():
    return render_template("pastr.html")

app,api = create_app()

if __name__ == '__main__':
    app.run("0.0.0.0", debug=True)